const puppeteer = require('puppeteer');
const authorization = require('./lib/authorization');
const downloadImages = require('./lib/downloadImages');
const getNeedsImages = require('./lib/getNeedsImages');

module.exports = async function(data, cookie) {
  console.clear();
  console.log('Start parse vk page...');

  const browser = await puppeteer.launch({
    args: ['--no-sandbox']
  });

  const page = await browser.newPage();
  const pageUrl = 'https://vk.com';
  const link = data.link;

  const result = {
    images: []
  };

  try {
    await page.goto(pageUrl);
    await page.setViewport({ width: 1366, height: 768 });
    await authorization(page, cookie);
    await page.goto(link);
    page.on('console', consoleObj => console.log(`--- ${consoleObj.text()}`));

    const getNeedsImagesResult = await getNeedsImages({page, link}, data.date);

    if (Array.isArray(getNeedsImagesResult) && getNeedsImagesResult.length) {
      result.images = getNeedsImagesResult;
      console.log('Download images...');
      await downloadImages(getNeedsImagesResult);
    } else {
      if (Array.isArray(getNeedsImagesResult) && !getNeedsImagesResult.length) result.message = 'Изображение за выбранный интервал не найдены';
      else result.message = getNeedsImagesResult;
    }
  } catch (error) {
    console.log(error);

    result.message = error;
  }

  await browser.close();
  console.log('Done.');
  return result;
};
