const puppeteer = require('puppeteer');
const authorization = require('./lib/authorization');

module.exports = async function(data) {
  console.clear();
  console.log('Authorization...');

  const browser = await puppeteer.launch({
    args: ['--no-sandbox']
  });

  const page = await browser.newPage();
  const pageUrl = 'https://vk.com';
  const result = {};

  try {
    await page.goto(pageUrl);
    const auth = await authorization(page, data, true);
    
    result.name = auth.name;
    result.avatar = auth.avatar;
    result.cookie = auth.cookie;
  } catch (error) {
    result.message = error;
  }

  await browser.close();
  console.log('Done.');
  return result;
};
