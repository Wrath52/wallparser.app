const fs = require('fs');
const files = require('./files');
const cookies = require('./cookies');

const loginSelector = '#index_email';
const passwordSelector = '#index_pass';
const profileSelector = '#top_profile_link';
const buttonSelector = '#index_login_button';

module.exports = async function(page, data, isAuth) {
  if (isAuth) {
    const login = await page.$(loginSelector);
    const password = await page.$(passwordSelector);

    await login.type(data.login);
    await password.type(data.password);
    await page.click(buttonSelector);
    await page.waitForSelector(profileSelector, { timeout: 10000 });

    const cookie = await cookies.getCookies(page);

    const avatar = await page.evaluate(function() {
      return document.querySelector(`#top_profile_link .top_profile_img`).src;
    });

    const name = await page.evaluate(function() {
      return document.querySelector(
        `#top_profile_link .top_profile_name`
      ).innerHTML;
    });

    return {
      name,
      avatar,
      cookie
    };
  } else {
    await cookies.setCookies(page, data);
    await page.evaluate(function() {
      location.reload(true);
    });

    await page.waitForSelector(profileSelector, { timeout: 10000 });
  }
};
