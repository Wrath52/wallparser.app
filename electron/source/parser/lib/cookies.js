async function getCookies(page) {
  const { cookies } = await page._client.send('Network.getAllCookies', {});
  return cookies;
}

async function setCookies(page, cookies) {
  const items = cookies
    .map(function(cookie) {
      const item = Object.assign({}, cookie);
      if (!item.value) item.value = '';
      return item;
    })
    .filter(function(cookie) {
      return cookie.name;
    });

  await page.deleteCookie(...items);

  if (items.length)
    await page._client.send('Network.setCookies', { cookies: items });
}

module.exports = {
  getCookies,
  setCookies
};
