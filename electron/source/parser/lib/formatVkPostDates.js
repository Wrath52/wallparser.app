const moment = require('moment');
require('moment/locale/ru');

function formatDate(date) {
  const isNowYear = date.search(' в ') !== -1;
  const isToday = date.toLowerCase().search('сегодня') !== -1;
  const isYesterday = date.toLowerCase().search('вчера') !== -1;

  let value;

  if (isToday || isYesterday) {
    const index = date.indexOf(':');
    const hour = date.slice(index - 2, index);
    const minute = date.slice(index+1, index + 3);

    if (isToday) {
      value = moment().set({ hour, minute, second: 0, millisecond: 0 }).toISOString();
    } else {
      value = moment().subtract('1', 'days').set({ hour, minute, second: 0, millisecond: 0 }).toISOString();
    }
  } else {
    const format = isNowYear ? 'DD MMM HH:mm' : 'DD MMM YYYY';
    value = moment(date, format).toISOString();
  }

  return {
    value,
    label: date
  };
}

function formatVkPostDates(data) {
  if (Array.isArray(data)) {
    return data.map(function(date) {
      return formatDate(date);
    });
  }

  return formatDate(data);
}

module.exports = formatVkPostDates;
