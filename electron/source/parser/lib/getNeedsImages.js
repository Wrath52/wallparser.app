const formatVkPostDates = require('./formatVkPostDates');

function wait(time) {
  return new Promise(function(res) {
    setTimeout(function() {
      res();
    }, time);
  });
}

async function scrollPageToNeedsPosts(page, date) {
  let scroll = 5000;
  let condition = false;

  while (!condition) {
    console.log('Scroll page...');
    const posts = await getPosts(page);
    const lastPostDate = posts[posts.length - 1].date.value;

    if (lastPostDate > date.from) {
      scroll += 5000;

      await page.evaluate(function(scroll) {
        window.scrollTo(0, scroll);
      }, scroll);

      await wait(5000);
    } else {
      condition = true;
    }
  }
}

async function waitUserWall(page) {
  const selector = '#profile_wall';
  await page.waitForSelector(selector);
}

async function waitReload(page) {
  await page.evaluate(function() {
    location.reload(true);
  });
}

async function getPosts(page, date) {
  const selector = '#profile_wall ._post.post';
  
  const posts = await page.$$eval(selector, function(selector) {
    const imageSelector = `.wall_text .image_cover`;

    return selector.map(function(post) {
      const id = post.id;
      const imagesCount = post.querySelectorAll(imageSelector).length;
      const date = post.querySelector('.post_date .rel_date').innerHTML;

      return { id, date, imagesCount };
    });
  });

  const result = posts.map(a => ({
    id: a.id,
    imagesCount: a.imagesCount,
    date: formatVkPostDates(a.date)
  }));

  if (date) {
    const filteredResult = [];

    result.forEach(a => {
      if (a.date.value <= date.to && a.date.value >= date.from) {
        filteredResult.push(a);
      }
    });

    return filteredResult;
  }

  return result;
}

async function getImages(page, posts) {
  const result = await page.evaluate(async function(posts) {
    const images = [];
    const imageSelector = `.wall_text .image_cover`;

    for (let i = 0; i < posts.length; i++) {
      const post = posts[i];
      const postContainer = document.getElementById(post.id);
      if (!postContainer) {
        console.log(`${post.id} is not FOUND!!!`);
        console.log(`${post.id} is not FOUND!!!`);
        console.log(`${post.id} is not FOUND!!!`);
        console.log(`${post.id} is not FOUND!!!`);
        console.log(`${post.id} is not FOUND!!!`);
        continue;
      }

      const postImages = postContainer.querySelectorAll(imageSelector);

      if (postImages && postImages.length) {
        for (let j = 0; j < postImages.length; j++) {
          const image = postImages[j];
          const data = {
            source: null,
            onclick: null,
            date: post.date
          };

          if (image.onclick) {
            data.onclick = `${image.onclick.toString()}; onclick();`;
          } else {
            data.source = getComputedStyle(image)['background-image'];
          }

          images.push(data);
        }
      }
    }

    return images;
  }, posts);

  return result;
}

async function getImagesSource({page, link}, images) {
  const result = [];

  for (let g = 0; g < images.length; g++) {
    let source;
    const { onclick, date } = images[g];

    if (onclick) {
      try {
        await page.goto(link);
        await waitUserWall(page);

        await page.evaluate(async function(onclick) {
          const image = document.createElement('img');
          image.onclick = new Function(onclick);
          image.click();
        }, onclick);

        await page.waitForSelector('#pv_photo img');
        source = await page.evaluate(function() {
          const selector = document.querySelector('#pv_photo img');
          return selector.src;
        });
      } catch (error) {
        return error;
      }
    } else if (postsImages[g].source) {
      source = postsImages[g].source;
    } else {
      return 'error parse image - no have onclick or source';
    }

    result.push({
      source,
      date
    });
  }

  return result;
}

module.exports = async function({ page, link }, date) {
  await waitUserWall(page);
  const initialPosts = await getPosts(page);
  if (initialPosts.length === 0) return 'Стена пользователя пуста';

  await scrollPageToNeedsPosts(page, date);

  const posts = await getPosts(page, date);
  const images = await getImages(page, posts);
  const result = await getImagesSource({page, link}, images);

  // console.log(posts);
  // console.log('============');
  // console.log('============');
  // console.log('============');
  // console.log('============');
  // console.log('============');
  // console.log(images);
  // console.log('============');
  // console.log('============');
  // console.log('============');
  // console.log('============');
  // console.log('============');
  // console.log(result);
  return result;
};
