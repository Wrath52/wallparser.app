const fs = require('fs');
const moment = require('moment');
const request = require('request');
const electron = require('electron');
const desktopPath = electron.app.getPath('desktop');

function callbackSaveFile(resolve) {
  return function(err) {
    if (err) console.log(err);
    else console.log('Photo save...');
    resolve();
  };
}

async function saveFile(uri, fileName, callback) {
  return new Promise(function(resolve) {
    request(uri, { encoding: 'binary' }, async function(err, response, body) {
      const contentType = response.headers['content-type'];
      let extension = contentType.split('/')[1];
      if (extension === 'jpeg') extension = 'jpg';
      const name = `${fileName}.${extension}`;

      fs.writeFile(name, body, 'binary', callback(resolve));
    });
  });
}

async function downloadImages(images) {
  if (!images.length) return;
  
  const date = moment();
  const day = moment(date).format('YYYY-MM-DD');
  const time = moment(date).format('HH-mm-ss');

  const saveDirectoryName = `wallParser ${day} ${time}`;
  const saveDirectoryPath = `${desktopPath}/${saveDirectoryName}`;
  
  if (!fs.existsSync(saveDirectoryPath)) {
    fs.mkdirSync(saveDirectoryPath, 0744);
  }

  for (let i = 0; i < images.length; i++) {
    const image = images[i];
    const url = image.source.replace(/(^url\()|"|\)/g, '');
    const fileName = `${saveDirectoryPath}/[${i+1}]-${image.date.label.replace(/\:/g, '-')}`;
    await saveFile(url, fileName, callbackSaveFile);
  }
}

module.exports = downloadImages;
