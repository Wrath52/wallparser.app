const fs = require('fs');

function getFileName(fileDirectory, fileName, fileExtension, withDate) {
  const fileDate = withDate ? new Date().toISOString() : '';
  return `${fileDirectory}/${fileName}${fileDate}.${fileExtension}`;
}

function getFileContent(file) {
  return new Promise(function(res) {
    try {
      fs.readFile(file, function(err, data) {
        res(JSON.parse(data.toString()));
      });
    } catch {
      res({ message: 'error' });
    }
  });
}

module.exports = { getFileName, getFileContent };
