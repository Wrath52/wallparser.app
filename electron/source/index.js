const path = require('path')
const { app, BrowserWindow, ipcMain } = require('electron')
const vkParser = require('./parser');
const authorization = require('./parser/authorization');
const Store = require('./store/store');
const createMenu = require('./menu');
const url = require('url');

const store = new Store({
  configName: 'user-preferences'
});

let mainWindow

const startUrl = process.env.ELECTRON_START_URL || url.format({
  pathname: path.join(__dirname, '../build/index.html'),
  protocol: 'file:',
  slashes: true,
});

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 800,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })

  mainWindow.loadURL(startUrl);
  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

app.on('ready', () => {
  createWindow();
  createMenu(() => mainWindow.webContents);

  try {

    ipcMain.on('getImages', async (event, data) => {
      const response = await vkParser(data, store.get('user').cookie);
      event.returnValue = response;
    });

    ipcMain.on('getUser', async event => {
      event.returnValue = store.get('user');
    });

    ipcMain.on('logout', async event => {
      store.delete('user');
      event.returnValue = store.get('user') || {};
    });

    ipcMain.on('authorization', async (event, data) => {
      const user = await authorization(data);

      if (user.message) {
        event.returnValue = {
          message: 'Ошибка авторизации. Проверьте данные и попробуйте снова'
        };
      } else {
        store.set('user', user);
        event.returnValue = store.get('user');
      }
    });

  } catch {
    mainWindow = null
  }
});

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})