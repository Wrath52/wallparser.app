const { app, Menu } = require('electron');

function createMenu(getContext) {
  const application = {
    label: 'Приложение',
    submenu: [
      {
        label: 'О приложении',
        selector: 'orderFrontStandardAboutPanel:',
      },
      {
        label: 'Режим разработчика',
        accelerator: 'CmdOrCtrl+J',
        click: () => {
          try {
            getContext().openDevTools();
          } catch (e) {
            // ignore
          }
        },
      },
      {
        type: 'separator',
      },
      {
        label: 'Закрыть',
        accelerator: 'Command+Q',
        click: () => {
          app.quit();
        },
      },
    ],
  };

  const edit = {
    label: 'Редактирование',
    submenu: [
      {
        label: 'Отменить',
        accelerator: 'CmdOrCtrl+Z',
        selector: 'undo:',
      },
      {
        label: 'Повторить',
        accelerator: 'Shift+CmdOrCtrl+Z',
        selector: 'redo:',
      },
      {
        type: 'separator',
      },
      {
        label: 'Вырезать',
        accelerator: 'CmdOrCtrl+X',
        selector: 'cut:',
      },
      {
        label: 'Копировать',
        accelerator: 'CmdOrCtrl+C',
        selector: 'copy:',
      },
      {
        label: 'Вставить',
        accelerator: 'CmdOrCtrl+V',
        selector: 'paste:',
      },
      {
        label: 'Выделить все',
        accelerator: 'CmdOrCtrl+A',
        selector: 'selectAll:',
      },
    ],
  };

  const template = [
    application,
    edit,
  ];
  Menu.setApplicationMenu(Menu.buildFromTemplate(template));
}

module.exports = createMenu;
