const { clipboard, ipcRenderer, remote } = require('electron');

window.open = require('open');
window.remote = remote;
window.pushConstants = require('electron-push-receiver/src/constants');

window.isElectron = true;
window.ipcRenderer = ipcRenderer;
window.electronClipboard = clipboard;
window.__DEV__ = Boolean(process.env.ELECTRON_START_URL);


// const path = require('path');

// const inputMenu = require('./inputContextMenu');
// const context = require('electron-contextmenu-middleware');

// context.use(inputMenu);
// context.activate();