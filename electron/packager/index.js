// const fs = require('fs');
const packager = require('electron-packager');
const createDMG = require('electron-installer-dmg');
const electronInstaller = require('electron-winstaller');

const appName = 'WallParser';

const options = {
  dir: './electron',
  appVersion: '1.0.3',
  osxSign: true,
  // {
  // identity: "Vlad Koshechkin (2FM7Z9P7DS)",
  // },
  asar: false,
  platform: 'win32, darwin',
  overwrite: true,
  icon: './electron/source/assets/icon',
  out: './electron/packager/build',
  appBundleId: 'ru.WallParser',
  name: appName
};

function createMacInstaller(path) {
  return new Promise(resolve => {
    createDMG(
      {
        appPath: path,
        name: `${appName}-installer`,
        background: './electron/source/assets/background.jpg',
        icon: './electron/source/assets/icon.png',
        overwrite: true,
        out: './electron/packager/installers/'
      },
      function done(err) {
        console.log(err);
        resolve(err);
      }
    );
  });
}

function createWinInstaller(path) {
  return new Promise(resolve => {
    electronInstaller.createWindowsInstaller(
      {
        appDirectory: path,
        exe: `${appName}.exe`,
        authors: 'Egor Kalianov',
        setupIcon: './electron/source/assets/icon.ico',
        outputDirectory: `./electron/packager/installers/`
      },
      function done(err) {
        console.log(err);
        resolve(err);
      }
    );
  });
}

packager(options).then(async appPaths => {
  // fs.emptyDirSync('./electron/packager/installers/');

  console.log('creating macOS installer');
  await createMacInstaller(`${appPaths[1]}/${appName}.app`);
  console.log('macOS installer created');

  console.log('creating windows installer');
  await createWinInstaller(appPaths[0]);
  console.log('windows installer created');

  console.log('DONE');
});
