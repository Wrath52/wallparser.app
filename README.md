# Electron + react js

## База

### Как завести

 - Скопировать `electron-entry.js`
 - `npm i --D electron`
 - `npm i -g electron-packager`
 - В `package.json` дописать:

```
  "homepage": "./",
  "main": "src/electron-entry.js",
  ...
  "scripts": {
    "electron": "electron .",
    "electron-dev": "ELECTRON_START_URL=http://localhost:3000 electron .",
    "pack-app": "electron-packager ./ --overwrite",
    ...
  }
```

### Разработка
```
 npm start
 npm run electron-dev
```

### Тест релиза

```
 npm run build
 npm run electron
```

### Упаковка релиза

Добавить в react/package.json 

```
 "homepage": "./",
```

```
 npm run build
 npm run pack
```

## sass
- В терминале

```
 npm run eject
 npm i --D sass-loader node-sass
```
- В файл `webpack.config.prod.js` дописать под `css`:

```
  {
    test: /\.scss$/,
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [
        require.resolve('css-loader'),
        require.resolve('sass-loader'),
      ],
    }),
  },
```
- В файл `webpack.config.dev.js` дописать под `css`:

```
  {
    test: /\.scss$/,
    use: [
      require.resolve('style-loader'),
      require.resolve('css-loader'),
      require.resolve('sass-loader'),
    ],
  },
```
