import React from 'react';
import ReactDOM from 'react-dom';

import Form from './components/Form';
import Input from './components/Input';
import Splash from './components/Splash';
import RangeDatepicker from './components/RangeDatepicker';

import './style.scss';
import moment from 'moment';

const splashTimeout = 2000;
let ipcRenderer = {
  sendSync: () => console.log('not electron variable')
};

if(window.ipcRenderer) ipcRenderer = window.ipcRenderer;

class App extends React.Component {
  state = {
    loading: false,
    userLink: '',
    splash: true,
    date: {
      from: moment().subtract(7, 'days'),
      to: moment()
    },
    user: {
      name: '',
      avatar: ''
    },
    authorization: {
      login: '',
      password: ''
    }
  };

  componentDidMount() {
    this.getUser();
    setTimeout(() => {
      this.setState({ splash: false });
    }, splashTimeout);
  }

  getUser = () => {
    const user = ipcRenderer.sendSync('getUser');

    if (user && user.name && user.avatar) {
      this.setState({
        user: {
          name: user.name,
          avatar: user.avatar
        }
      });
    }
  };

  handleInputChange = field => {
    if (field === 'login' || field === 'password') {
      return value => {
        this.setState({
          authorization: {
            ...this.state.authorization,
            [field]: value
          }
        });
      };
    }

    return value => {
      this.setState({
        [field]: value
      });
    };
  };

  handleDatepickerChange = (from, to) => {
    this.setState({
      date: {
        from,
        to
      }
    });
  };

  handleSubmit = async () => {
    if (this.state.loading) return;
    this.setState({ loading: true });

    setTimeout(async () => {
      const response = await ipcRenderer.sendSync('getImages', {
        date: {
          from: moment(this.state.date.from)
            .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
            .toISOString(),
          to: moment(this.state.date.to)
            .set({ hour: 23, minute: 59, second: 59, millisecond: 999 })
            .toISOString()
        },
        link: this.state.userLink
      });
      
      if (response.message) {
        alert(JSON.stringify(response.message));
      } else {
        alert(
          `Изображение за выбранный интервал скачаны. Кол-во: ${response.images.length}`
        );
      }

      this.setState({ loading: false });
    }, 100);
  };

  handleLogout = () => {
    const user = ipcRenderer.sendSync('logout');
    this.setState({ user });
  };

  handleAuthorization = async () => {
    if (this.state.loading) return;
    this.setState({ loading: true });

    setTimeout(async () => {
      const response = await ipcRenderer.sendSync(
        'authorization',
        this.state.authorization
      );

      if (response.message) {
        alert(response.message);
        return;
      } else if (response.name && response.avatar) {
        this.setState({
          user: {
            name: response.name,
            avatar: response.avatar
          }
        });
      }

      this.setState({ loading: false });
    }, 100);
  };

  renderLogout = () => {
    return (
      <div className="logout">
        <div className="logout__button" onClick={this.handleLogout}>
          Разлогиниться
        </div>
      </div>
    );
  };

  renderUser = () => {
    const { user } = this.state;

    return (
      <div className="user">
        <img src={user.avatar} alt="" className="user__avatar" />
        <div className="user__name">{user.name}</div>
      </div>
    );
  };

  renderAuthorizationForm = () => {
    const { authorization, loading } = this.state;

    return (
      <Form onSubmit={this.handleAuthorization}>
        <p>Авторизация в ВК</p>
        <Input
          required
          label="Логин"
          onChange={this.handleInputChange('login')}
          value={authorization.login}
        />
        <Input
          required
          type="password"
          label="Пароль"
          onChange={this.handleInputChange('password')}
          value={authorization.password}
        />
        <button>{loading ? 'Подождите...' : 'Авторизоваться'}</button>
      </Form>
    );
  };

  renderForm = () => {
    const { userLink, date, loading } = this.state;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Input
          label="Интервал дат"
          value={date.from}
          component={props => (
            <RangeDatepicker
              {...props}
              required
              onChange={this.handleDatepickerChange}
              from={date.from}
              to={date.to}
            />
          )}
        />
        <Input
          required
          label="ID пользователя"
          onChange={this.handleInputChange('userLink')}
          value={userLink}
        />
        <button>{loading ? 'Подождите...' : 'Скачать изображения'}</button>
      </Form>
    );
  };

  render() {
    const { splash, user } = this.state;
    if (splash) return <Splash />;

    if (!user.name) {
      return this.renderAuthorizationForm();
    }

    return (
      <React.Fragment>
        {this.renderUser()}
        {this.renderForm()}
        {this.renderLogout()}
      </React.Fragment>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('app'));
