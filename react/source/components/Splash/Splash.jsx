import React, { Component } from 'react';
import logo from '../../images/logo/svg.svg'
import Svg from '../Svg';
import './Splash.scss';

class Splash extends Component {
  getClassName = () => {
    let className = 'splash';
    if (this.props.className) className += ` ${this.props.className}`;

    return className;
  };

  render() {
    return (
      <div className={this.getClassName()}>
        <Svg image={logo} />
        <div className="title">WallParser</div>
      </div>
    );
  }
}
export default Splash;
