import React, { Component } from 'react';

class Form extends Component {
  onSubmit = event => {
    event.preventDefault();
    this.props.onSubmit();
  };

  render() {
    const { children, ...props } = this.props;
    return (
      <form {...props} onSubmit={this.onSubmit}>
        {children}
      </form>
    );
  }
}
export default Form;
