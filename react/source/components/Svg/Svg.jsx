import InlineSVG from 'svg-inline-react';
import React, { Component } from 'react';
import './Svg.scss';

class Svg extends Component {
  getClassName = () => {
    let className = 'svg-image';
    if (this.props.className) className += ` ${this.props.className}`;
    if (this.props.onClick) className += ' svg-image_clickable';

    return className;
  };

  render() {
    const { image, ...props } = this.props;

    return <InlineSVG {...props} className={this.getClassName()} src={image} />;
  }
}

export default Svg;
