import $ from 'jquery';
import React from 'react';
import getConfig from './config';

import moment from 'moment';

import 'daterangepicker';
import 'moment/locale/ru';
import './RangeDatepicker.scss';
import 'daterangepicker/daterangepicker.css';

const getRandom = () => String(Math.random()).split('.')[1];

class Datepicker extends React.Component {
  componentDidMount() {
    this._renderDatePicker();
  }

  componentDidUpdate(prevProps) {
    const { from, to } = this.props;

    if (from !== prevProps.from || to !== prevProps.to) {
      this._removeDatePicker();
      this._renderDatePicker();
    }

    return true;
  }

  componentWillUnmount() {
    this._removeDatePicker();
  }

  id = `daterangepicker${getRandom()}`;

  config = () => getConfig(this.props);

  containerRef = React.createRef();

  _removeDatePicker = () => {
    $(`[data-id="${this.id}"]`).remove();
  };

  _renderDatePicker = () => {
    // @ts-ignore
    $(`#${this.id} input`).daterangepicker(this.config(), this.handleChange);

    // @ts-ignore
    $(`#${this.id} input`).on('show.daterangepicker', this.onShow);
  };

  onShow = (e, picker) => {
    picker.container.attr('data-id', this.id);
  };

  handleChange = (from, to) => {
    this.props.onChange(from, to);
  };

  getDates = () => {
    return {
      startDate: this.props.from
        ? moment(this.props.from).format('DD/MM/YYYY')
        : moment(),
      endDate: this.props.to
        ? moment(this.props.to).format('DD/MM/YYYY')
        : moment()
    };
  };

  render() {
    return (
      <div
        ref={this.containerRef}
        className="rangedatepicker-container"
        id={this.id}
      >
        <input className="rangedatepicker-input" />
      </div>
    );
  }
}

export default Datepicker;
