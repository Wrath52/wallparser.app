import moment from 'moment';

const getConfig = (props) => ({
  ranges: {
    Сегодня: [moment(), moment()],
    Вчера: [
      moment().subtract(
        1, 'days'
      ), moment().subtract(
        1, 'days'
      )
    ],
    'За 7 дней': [
      moment().subtract(
        6, 'days'
      ), moment()
    ],
    // 'За 30 дней': [
    //   moment().subtract(
    //     29, 'days'
    //   ), moment()
    // ],
    'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
    'Последний месяц': [
      moment()
        .subtract(
          1, 'month'
        )
        .startOf('month'),
      moment()
        .subtract(
          1, 'month'
        )
        .endOf('month')
    ],
    // 'Посл. 3 месяца': [
    //   moment()
    //     .subtract(
    //       3, 'month'
    //     )
    //     .startOf('month'),
    //   moment()
    //     .subtract(
    //       1, 'month'
    //     )
    //     .endOf('month')
    // ],
    // 'Посл. год': [
    //   moment()
    //     .subtract(
    //       12, 'month'
    //     )
    //     .startOf('month'),
    //   moment()
    //     .subtract(
    //       1, 'month'
    //     )
    //     .endOf('month')
    // ],
    // 'Все время': [
    //   moment()
    //     .subtract(
    //       60, 'month'
    //     )
    //     .startOf('month'),
    //   moment()
    // ]
  },
  startDate: props.from ? moment(props.from).format('DD/MM/YYYY') : moment(),
  endDate: props.to ? moment(props.to).format('DD/MM/YYYY') : moment(),
  locale: {
    format: 'DD.MM.YYYY',
    separator: ' - ',
    applyLabel: 'Применить',
    cancelLabel: 'Отмена',
    fromLabel: 'От',
    toLabel: 'До',
    customRangeLabel: 'Своя дата',
    daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    monthNames: [
      'Январь',
      'Февраль',
      'Март',
      'Апрель',
      'Май',
      'Июнь',
      'Июль',
      'Август',
      'Сентябрь',
      'Октябрь',
      'Ноябрь',
      'Декабрь'
    ],
    firstDay: 1
  },
  alwaysShowCalendars: true
});

export default getConfig;
