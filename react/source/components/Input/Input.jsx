import React from 'react';
import './style.scss';

class Input extends React.PureComponent {
  getClassName = () => {
    const { className, value } = this.props;
    let cn = 'Input';
    if (className) cn += ` ${className}`;
    if (value || value === 0) cn += ' Input_active';
    return cn;
  };

  getId = () => {
    const string = Math.random()
      .toString(36)
      .replace(/\d|\./g, '');

    return string;
  };

  onChange = event => {
    const { value } = event.target;
    this.props.onChange(value);
  };

  render() {
    const id = this.getId();
    const { label, component } = this.props;
    const Input = component || 'input';


    const inputProps = { ...this.props, id };
    delete inputProps.component;

    return (
      <div className={this.getClassName()}>
        <Input
          {...inputProps}
          onChange={this.onChange}
          className="Input__TextField"
        />
        {label && <label htmlFor={id}>{label}</label>}
      </div>
    );
  }
}

export default Input;
