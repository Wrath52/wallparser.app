if (window.require) {
  const { ipcRenderer } = window.require('electron');
}

const callElectron = (eventName, type, ...arg) => {
  try {
    return ipcRenderer[type](eventName, ...arg);
  } catch (err) {
    console.log('callElectron work only electron app');
  }
};

export default callElectron;
